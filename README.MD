# Préambule

En l'état, ce plugin expérimental présente des difficultés d'usages et des bugs dissuasifs.
Ya un besoin, mais l'inconfort à faire "sans" ne motive pas assez le développement.
L'évolution de ce plugin est pour l'instant interrompue.

La suite est la copie de https://git.spip.net/spip/spip/issues/4983 qui a précédé la création de ce repo.
Le code doit correspondre plus ou moins.

### Le besoin
Il arrive que l'autorisation ou la non autorisation ait pour conséquence l'affichage d'un message, et qu'on ait besoin pour cela de savoir la raison précise ayant conduit à cette autorisation ou non autorisation.

Par exemple pour les refus :
- Cette partie du site est réservée aux visiteurs authentifiés. Veuillez vous connecter.
- Votre abonnement est terminé depuis 17 jours, vous ne pouvez plus accéder à cette partie.
- Passe ton bac d'abord !
- Vous avez déjà posté 53 messages en moins d'une heure. On va laisser refroidir internet.

### Le problème
Pour qu'un squelette SPIP fournisse ce message adapté, il doit connaître la raison précise qui motive l'autorisation ou le refus, et pour cela actuellement il faut reproduire en SPIP l'algoritme PHP déployée par autoriser et ses potentielles spécialisations et surcharges, ce qui peut donc être complexe, voire impossible puisque c'est surchargeable en PHP, et pas de base prévu en SPIP.

### L'envie

Ce serait donc sympa et puissant de pouvoir savoir, en SPIP, la raison qui motive  une autorisation ou une non autorisation, SANS devoir dupliquer en SPIP le code PHP qui a conduit a cette conclusion.

Les "explications" fournies pourraient être des chaines explicatives, mais aussi des noms de squelettes qui seraient chargés de  fournir le feedback approprié avec sa mise en forme appropriée, [edit] ou des tableaux associatifs fournissant plus d'informations sur les circonstances qui justifient d'accorder ou non 'autorisation (détaillé plus loin.

```
[(#AUTORISER{voir, article, 2}|non) 
    <div class="refus">#AUTORISER_EXPLIQUER{voir,article,2}
    </div>]
[(#AUTORISER{voir, article, 2}|non) 
    <INCLURE{fond=feedback/#AUTORISER_EXPLIQUER{voir,article,2},env}>
]    
```

Lorsque la valeur mémorisée est un tableau associatif, cela permet d'y mettre les détails de l'expertise qui amène à finalement accorder ou non l'autorisation, comme un nouveau contexte, et de s'en servir pour délivrer le feedback sans refaire toute l'analyse (il peut rester des traitements pour la mise en forme, mais c'est simplifié).

```
[(#AUTORISER{voir, article, 2}|non) 
	#SET{explic,#AUTORISER_EXPLIQUER{voir,article,2}}
    <INCLURE{fond=feedback/#GET{explic/squelette},age_peremption=#GET{explic/age_peremption},type_proposition=#GET{explic/type_proposition}>
]
```

J'ai évoqué l'usage en SPIP mais potentiellement il peut y avoir besoin d'explications en PHP aussi.

### Piste

- le calcul d'une autorisation garde en mémoire ses résultats y compris une explication associée, fournie *en plus*, ou pas, par le code final qui comme actuellement renvoie true ou false.

- une nouvelle valeur d'option de `autoriser()` et `#AUTORISER` permet de récupérer en SPIP l'explication associée à un appel précédent. Ou alors ce sont de nouvelles fonctions `autoriser_expliquer()` et `#AUTORISER_EXPLIQUER` avec la même signature que  `autoriser` et `#AUTORISER`

- c'est mémorisé dans une static locale [comme $Memoization](https://git.spip.net/spip-contrib-extensions/memoization/src/branch/master/memoization_options.php#L234) accédée via une fonction `autoriser_explication()`. 

- Ça contient un tableau dont les index sont une signature de l'appel (un hash ?) et dont les valeurs sont un couple {résultat booléen, explication}.

- c'est un objet avec des méthodes : `set` et `get_explication` principalement. 

- Avant de renvoyer un $ok true ou false comme actuellement, une autorisation qui veut pouvoir fournir des explications appelle `autoriser_explication()->set($signature, $ok, $explication)` pour mettre l'explication à disposition d'une éventuelle future demande d'explication.

- `#AUTORISER_EXPLIQUER` appelle et renvoie `autoriser_explication()->get_explication ($signature)`

Yaurait des limites et responsabilité du squelette ou d'un PHP appelant :
  * Demander l'explication dans le même hit que l'autorisation, et faire en sorte que le résultat soit d'actualité (ou alors mémoriser de manière plus durable)
  * évidemment ça ne fournit une explication que pour les autorisations qui en fournissent une.

**Nomenklatura** : faut pas se mélanger entre les `_expliquer` et les `_explication`. Peut être ne retenir que `_expliquer` partout.

Cf initialement : https://discuter.spip.net/t/spip-zone-api-de-non-autorisation-presenter-un-refus/144812

