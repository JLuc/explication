<?php

function balise_EXPLIQUER_dist($p) {
	$_code = [];
	
	$n = 1;
	while ($_v = interprete_argument_balise($n++, $p)) {
		$_code[] = $_v;
	}
	
	$p->code = 'expliquer(' . join(
			', ',
			$_code
		) . ')';
	$p->interdire_scripts = false;
	
	return $p;
}

function expliquer(string $faire, ?string $type='', int $id=0, ?array $qui=[], ?array $opt=[]) {
	return explications()->explique ($faire, $type, $id, $qui, $opt);
}