<?php
function balise__EXPLIQUER_dist($p) {
	$p->interdire_scripts = false;

	// Appelé uniquement au recalcul
	$p->code = V_OUVRE_PHP. 'echo '.compile_appel_macro_expliquer ($p).';'.V_FERME_PHP;
	if (debug_get_mode ('expliquer')) {
		echo "compile_appel_macro_expliquer:<pre>{$p->code}</pre>";
	}
	return $p;
}

/**
 * @param $p
 * @return string
 *
 * Appelé uniquement au recalcul
 */
function compile_appel_macro_expliquer ($p): string {
	if (!existe_argument_balise(1, $p)) {
		erreur_squelette ("Il faut au moins un argument à la balise #_EXPLIQUER", $p);
		return "''";
	}
	$faire = interprete_argument_balise(1, $p);
	if (erreur_argument_macro ('#_EXPLIQUER', 'faire', $faire, $p)) {
		return "''";
	}
	// l'autorisation peut être appelé avec 0, un ou 2 arguments
	if (!existe_argument_balise(2, $p)) {
		return "expliquer ('.\"$faire\".')";
	}

	$type = trim_quote(interprete_argument_balise (2, $p));
	if (erreur_argument_macro ("#_EXPLIQUER{{$faire},...}", 'type', $type, $p)) {
		return "''";
	}
	if (!existe_argument_balise(3, $p)) {
		return "expliquer('.\"$faire\".', '.\"$type\".')";
	}

	// Le 3eme argument doit être une constante ou un appel direct de #BALISE (sans traitement) ou #GET{variable}
	// Ex : #_EXPLIQUER{modifier,article,10} ou #_EXPLIQUER{modifier,article,#GET{alaune}} ou #_EXPLIQUER{modifier,article,#ID_ARTICLE}
	$id = trim_quote(interprete_argument_balise (3, $p));
	if (!existe_argument_balise(4, $p)) {
		// Préparer l'expression compilée à être réinjectée (code introspectivement modifié)
		$id = reinjecte_expression_compilee($id);

		// TODO : Récupérer les erreurs de syntaxe
		return "expliquer('.\"$faire\".', '.\"$type\".', '.\"$id\".')";
	}

	// ATTENTION Les appels à #_EXPLIQUER avec arguments $qui et $opt n'ont jamais été testés 
	$qui = trim_quote(interprete_argument_balise (4, $p));
	if (erreur_argument_macro ("#_EXPLIQUER{ $faire, $type, $id, ...}", 'qui', $qui, $p))
		return "''";
	if (!existe_argument_balise(5, $p))
		return "expliquer('.\"$faire\".', ' .\"$type\" .', ' .\"$id\" .', ' .\"$qui\" .')";

	$opt = trim_quote(interprete_argument_balise (5, $p));
	if (erreur_argument_macro ('#_EXPLIQUER', 'opt', $opt, $p))
		return "''";
	return "expliquer('.\"$faire\".', '.\"$type\" .', ' .\"$id\" .', ' .\"$qui\" .', ' .\"$opt\" .')";
}


