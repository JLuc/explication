<?php

include_spip('inc/expliquer');
if (test_plugin_actif ('macrosession')) {
	include_spip ('inc/_expliquer');
}
elseif (debug_get_mode('expliquer')) {
	echo "macrosession n'est pas actif<br>";
}

class Explications {
	private static $explications = [];
	/**
	 *
	 */
	public function __construct () {

	}

	/**
	 * @param string $type
	 * @param int $id
	 * @param null|int|array $qui
	 * @param ?array $opt
	 */
	private static function get_cle (string $faire, string $type, int $id=0, $qui='', ?array $opt=[]): string {
		if (!$opt) {
			$opt = [];  // unifier la signature
		}
		return md5 (serialize ([$faire, $type, $id, $qui, $opt]));
	}

	/**
	 * @param $explication
	 * @param string $type
	 * @param int $id
	 * @param ?array $qui
	 * @param ?array $opt
	 * @return void
	 */
	public function memorise ($explication, string $faire, string $type, int $id=0, ?array $qui=[], ?array $opt=[]) {
		if (debug_get_mode ('expliquer')) {
			echo "memorise <i>$explication</i> pour faire=$faire type=$type id=$id...<br>";
			echo '-- clé=' . self::get_cle ($faire, $type, $id, $qui, $opt) . '<br>';
		}

		self::$explications[self::get_cle ($faire, $type, $id, $qui, $opt)] = $explication;
		if (debug_get_mode ('expliquer')) {
			echo 'verif : ' . (self::explicable ($faire, $type, $id, $qui, $opt) ? 'OK' : 'C’EST INEXPLICABLE !') . '<br>';
		}
	}

	/**
	 * @return mixed
	 * @param string $type
	 * @param int $id
	 * @param null|int|array $qui
	 * @param array $opt
	 */
	public function explique (string $faire, string $type, int $id=0, ?array $qui=[], ?array $opt=[]) {
		$cle = self::get_cle($faire, $type, $id, $qui, $opt);
		if (debug_get_mode ('expliquer')) {
			echo "explique $faire, $type, $id...<br>";
			echo "-- clé=$cle<br>";
			if ($_GET['dump'] ?? '') {
				echo '<pre>';
				var_dump (self::$explications);
				echo '</pre>';
			}
			echo (isset(self::$explications[$cle]) ? 'Bien connu !<br>' : 'Inconnu !<br>');
		}
		return self::$explications[$cle] ?? 'PAS D\'EXPLICATION';
	}

	/**
	 * @return mixed
	 * @param string $type
	 * @param int $id
	 * @param null|int|array $qui
	 * @param array $opt
	 */
	public function explicable (string $faire, string $type, int $id=0, $qui='', array $opt=[]) {
		return isset(self::$explications[self::get_cle($faire, $type, $id, $qui, $opt)]);
	}
}

/**
 * Interface principale d'utilisation de Explication
 * @return Explications
 */
function explications(): Explications {
	static $Explications = null;
	if (is_null($Explications)){
		$Explications = new Explications();
		if (debug_get_mode ('expliquer')) {
			echo "Explications initialisees<br>";
		}
	}
	return $Explications;
}

/*
 * Pour tests 
*/

if (!function_exists('debug_get_mode')) {
	/**
	 * @param string $part : fonctionnalité testée
	 * @return bool : si l'argument debug est passé et égal à la fonctionnalité testée
	 * exemple : if (debug_get_mode('facteur')) echo $expediteur;
	 */
	function debug_get_mode($part = '')
	{
		return isset($_GET['debug'])
			and (!$part or ($_GET['debug'] == $part));
	}
}

if (debug_get_mode('explitests')) {
	explications ()->memorise ("une première explic", 'tester', 'code');
	echo '<b>"tester,code"</b> est ' . (explications ()->explicable ('tester', 'code') ? 'explicable' : 'pas explicable') . ' :';
	echo explications ()->explique ('tester', 'code') . '<br>';
	echo '"tester2,code" est ' . (explications ()->explicable ('tester2', 'code') ? 'explicable' : 'NON explicable') . ' : ';
	echo explications ()->explique ('tester2', 'code') . '<br>';
	echo '------------------------------------------------------<br>';
}
